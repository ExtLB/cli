import * as ejs from 'ejs';

export default {
  'Gruntfile.js': (options: any, content: any) => {
    content = ejs.render(content, {options: options});
    return content;
  }
};
