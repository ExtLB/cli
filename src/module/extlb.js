"use strict";
exports.__esModule = true;
exports["default"] = {
    'extlb.json': function (options, content) {
        content.name = "extlb-module-" + options.name;
        content.client = options.client;
        content.server = options.server;
        content.locale = "mod-" + options.name;
        return content;
    }
};
//# sourceMappingURL=extlb.js.map