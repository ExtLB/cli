#!/usr/bin/env node
require('babel-register')({
});
import * as ExtLB from '..';
import * as yargs  from 'yargs';

console.log(ExtLB.default.default.logo);

class ELB {
  constructor() {
    yargs.commandDir('../cmds').demandCommand().help().argv;
  }
}
new ELB();
