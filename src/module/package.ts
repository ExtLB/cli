import * as _ from 'lodash';

export default {
  'package.json': (options: any, content: any) => {
    content.name = `extlb-module-${options.name}`;
    content.description = `${_.capitalize(options.name)} Module for ExtLoopback`;
    content.keywords.push(_.capitalize(options.name));
    return content;
  }
};
