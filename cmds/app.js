"use strict";
exports.__esModule = true;
var ExtLB = require("..");
var Inquirer = require("inquirer");
var path = require("path");
var _ = require("lodash");
var Joi = require("joi");
var App = (function () {
    function App() {
        this.options = {
            name: null,
            camelName: null,
            snakeName: null,
            capitalName: null,
            description: null,
            version: null,
            authType: null,
            platforms: null,
            features: null
        };
    }
    App.prototype.getName = function () {
        var me = this;
        var curDir = _.last(process.cwd().split(path.sep));
        return Inquirer.prompt([{
                type: 'input',
                name: 'name',
                message: 'Application Name',
                "default": me.options.name || curDir.toLowerCase(),
                validate: function (name) {
                    if (_.isNull(Joi.validate(name, Joi.string().lowercase().required(), { convert: false }).error)) {
                        return true;
                    }
                    else {
                        return 'Application Name must be lowercase';
                    }
                }
            }]).then(function (answers) {
            me.options.name = answers.name;
            me.options.camelName = _.camelCase(answers.name);
            me.options.snakeName = _.snakeCase(answers.name);
            me.options.capitalName = me.options.camelName.charAt(0).toUpperCase() + me.options.camelName.substr(1);
        });
    };
    App.prototype.getDescription = function () {
        var me = this;
        var curDir = _.last(process.cwd().split(path.sep));
        return Inquirer.prompt([{
                type: 'input',
                name: 'description',
                message: 'Application Description',
                "default": me.options.name || curDir.toLowerCase()
            }]).then(function (answers) {
            me.options.description = answers.description;
        });
    };
    App.prototype.getVersion = function () {
        var me = this;
        return Inquirer.prompt([{
                type: 'rawlist',
                name: 'version',
                message: 'ExtLoop Version',
                "default": '1',
                choices: ExtLB["default"]["default"].versions
            }]).then(function (answers) {
            me.options.version = answers.version;
        });
    };
    App.prototype.getPlatforms = function () {
        var me = this;
        return Inquirer.prompt([{
                type: 'checkbox',
                name: 'platforms',
                message: 'Platform(s)',
                choices: ExtLB["default"]["default"].platforms
            }]).then(function (answers) {
            me.options.platforms = answers.platforms;
        });
    };
    App.prototype.getFeatures = function () {
        var me = this;
        return Inquirer.prompt([{
                type: 'checkbox',
                name: 'features',
                message: 'Enable Features',
                pageSize: 20,
                choices: ExtLB["default"]["default"].features
            }]).then(function (answers) {
            me.options.features = answers.features;
        });
    };
    App.prototype.getAuthentication = function () {
        var me = this;
        return new Promise(function (resolve, reject) {
            if (!_.includes(me.options.features, 'auth')) {
                resolve();
            }
            else {
                Inquirer.prompt([{
                        type: 'rawlist',
                        name: 'authType',
                        message: 'Authentication Type',
                        "default": '1',
                        choices: ExtLB["default"]["default"].authentication
                    }]).then(function (answers) {
                    me.options.authType = answers.authType;
                    resolve();
                })["catch"](function (err) {
                    reject(err);
                });
            }
        });
    };
    App.prototype.getPassportJS = function () {
        var me = this;
        return new Promise(function (resolve, reject) {
            if (!_.includes(me.options.features, 'auth')) {
                resolve();
            }
            else {
                Inquirer.prompt([{
                        type: 'confirm',
                        name: 'passportjs',
                        message: 'Would you like to use passportJS for authentication?',
                        "default": false
                    }]).then(function (answers) {
                    if (answers.passportjs === true) {
                        me.options.features.push('passportjs');
                    }
                    resolve();
                })["catch"](function (err) {
                    reject(err);
                });
            }
        });
    };
    return App;
}());
exports.command = 'app';
exports.describe = 'Create a new ExtLoop Application';
exports.builder = {};
exports.handler = function (argv) {
    var app = new App();
    app.getName().then(function () {
        return app.getDescription();
    }).then(function () {
        return app.getVersion();
    }).then(function () {
        return app.getPlatforms();
    }).then(function () {
        return app.getFeatures();
    }).then(function () {
        return app.getAuthentication();
    }).then(function () {
        return app.getPassportJS();
    }).then(function () {
        ExtLB["default"]["default"].createApp(app.options);
    })["catch"](function (err) {
        console.error(err);
    });
};
//# sourceMappingURL=app.js.map