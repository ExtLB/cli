import * as ejs from 'ejs';

export default {
  'index.html': (options: any, content: any) => {
    content = ejs.render(content, {options: options});
    return content;
  }
};
