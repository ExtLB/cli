export default {
  'extlb.json': (options: any, content: any) => {
    content.name = `extlb-module-${options.name}`;
    content.client = options.client;
    content.server = options.server;
    content.locale = `mod-${options.name}`;
    return content;
  }
};
