import * as ejs from 'ejs';
import * as path from 'path';

let changes: any = {};
changes[`app${path.sep}desktop${path.sep}src${path.sep}controller${path.sep}Socket.js`] = (options: any, content: any) => {
  if (options.features.indexOf('socketio') === -1) {
    content = null;
  }
  return content;
};
changes['app.js'] = (options: any, content: any) => {
  content = ejs.render(content, {options: options});
  return content;
};
export default changes;
