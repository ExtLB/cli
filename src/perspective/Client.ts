import * as ejs from 'ejs';
import * as path from 'path';

let changes: any = {};
function ejsProcessor(options: any, content: any) {
  content = ejs.render(content, {options: options});
  return content;
}
changes[`app${path.sep}desktop${path.sep}src${path.sep}model${path.sep}perspective${path.sep}template${path.sep}NavigationItem.js`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}model${path.sep}perspective${path.sep}template${path.sep}Notification.js`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}View.js`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}ViewController.js`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}ViewModel.js`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}center${path.sep}View.js`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}center${path.sep}View.scss`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}footer${path.sep}View.js`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}footer${path.sep}View.scss`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}header${path.sep}View.js`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}header${path.sep}View.scss`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}nav${path.sep}View.js`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}nav${path.sep}View.scss`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}nav${path.sep}ViewController.js`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}nav${path.sep}bottom${path.sep}View.js`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}nav${path.sep}bottom${path.sep}View.scss`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}nav${path.sep}menu${path.sep}View.js`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}nav${path.sep}menu${path.sep}View.scss`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}nav${path.sep}top${path.sep}View.js`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}perspective${path.sep}template${path.sep}nav${path.sep}top${path.sep}View.scss`] = ejsProcessor;
export default changes;
