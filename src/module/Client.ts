import * as ejs from 'ejs';
import * as path from 'path';

let changes: any = {};
function ejsProcessor(options: any, content: any) {
  content = ejs.render(content, {options: options});
  return content;
}
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}template${path.sep}View.js`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}template${path.sep}View.scss`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}template${path.sep}ViewController.js`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}template${path.sep}ViewModel.js`] = ejsProcessor;
changes[`app${path.sep}desktop${path.sep}src${path.sep}view${path.sep}template${path.sep}ViewStore.js`] = ejsProcessor;
export default changes;
