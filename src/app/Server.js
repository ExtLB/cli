"use strict";
exports.__esModule = true;
var path = require("path");
var ejs = require("ejs");
var _ = require("lodash");
var changes = {};
changes["config" + path.sep + "server" + path.sep + "config.json"] = function (options, content) {
    if (!_.includes(options.features, 'socketio')) {
        delete content.socket;
    }
    if (!_.includes(options.features, 'queues')) {
        delete content.queues;
    }
    if (!_.includes(options.features, 'auth')) {
        content.auth.enabled = false;
    }
    if (!_.includes(options.features, 'passportjs')) {
        content.passport = false;
    }
    return content;
};
changes["config" + path.sep + "server" + path.sep + "model-config.json"] = function (options, content) {
    if (!_.includes(options.features, 'passportjs')) {
        var sources = content._meta.sources;
        content._meta.sources = [sources[0], sources[1], sources[2], sources[3]];
        delete content.applicationCredential;
        delete content.userCredential;
        delete content.userIdentity;
    }
    if (!_.includes(options.features, 'auth')) {
        delete content.accessToken;
        delete content.application;
        delete content.role;
        delete content.roleMapping;
        delete content.user;
    }
    return content;
};
var passportJSModels = ['application-credential', 'user-credential', 'user-identity'];
_.each(passportJSModels, function (passportJSModel) {
    changes["server" + path.sep + "models" + path.sep + passportJSModel + ".json"] = function (options, content) {
        if (!_.includes(options.features, 'passportjs')) {
            content = null;
        }
        return content;
    };
});
var coreAuthModels = ['access-token', 'application', 'role', 'role-mapping', 'user'];
_.each(coreAuthModels, function (coreAuthModel) {
    changes["server" + path.sep + "models" + path.sep + coreAuthModel + ".ts"] = function (options, content) {
        if (!_.includes(options.features, 'auth')) {
            content = null;
        }
        return content;
    };
    changes["server" + path.sep + "models" + path.sep + coreAuthModel + ".json"] = function (options, content) {
        if (!_.includes(options.features, 'auth')) {
            content = null;
        }
        return content;
    };
});
changes["server" + path.sep + "boot" + path.sep + "queues.ts"] = function (options, content) {
    if (!_.includes(options.features, 'queues')) {
        content = null;
    }
    return content;
};
changes["server" + path.sep + "boot" + path.sep + "socket.ts"] = function (options, content) {
    if (options.features.indexOf('socketio') === -1) {
        content = null;
    }
    return content;
};
changes["server" + path.sep + "server.js"] = function (options, content) {
    content = ejs.render(content, { options: options });
    return content;
};
exports["default"] = changes;
//# sourceMappingURL=Server.js.map