"use strict";
exports.__esModule = true;
var inquirer = require("inquirer");
var path = require("path");
var jsonfile = require("jsonfile");
var _ = require("lodash");
var async = require("async");
var fs = require("fs");
var shelljs = require("shelljs");
var ejs = require("ejs");
var chalk_1 = require("chalk");
var simpleGit = require('simple-git/promise');
var ExtLB = (function () {
    function ExtLB() {
        this.logo = chalk_1["default"].green("                 (          \n               ) )\\ )   (   \n (       )  ( /((()/( ( )\\  \n )\\   ( /(  )\\())/(_)))((_) \n((_)  )\\())(_))/(_)) ((_)_  \n| __|((_)\\ | |_ | |   | _ ) \n| _| \\ \\ / |  _|| |__ | _ \\ \n|___|/_\\_\\  \\__||____||___/ ");
        this.versions = ['v1'];
        this.authentication = [{ name: 'User', value: 'users' }, { name: 'Accounts', value: 'accounts' }];
        this.platforms = [{ name: 'desktop', checked: true }, { name: 'phone' }];
        this.features = [
            new inquirer.Separator('ExtLoop Features'), {
                name: 'Browserify',
                value: 'browserify',
                checked: true
            }, {
                name: 'Socket.IO',
                value: 'socketio',
                checked: true
            }, {
                name: 'Authentication',
                value: 'auth',
                checked: true
            }, {
                name: 'Queues',
                value: 'queues'
            }, new inquirer.Separator('ExtLoop Perspectives'), {
                name: 'Admin',
                value: 'perspective-admin',
                checked: true
            }, {
                name: 'Auth',
                value: 'perspective-auth',
                checked: true
            }, {
                name: 'Error',
                value: 'perspective-error',
                checked: true
            }, {
                name: 'Main',
                value: 'perspective-main',
                checked: true
            }
        ];
        this.models = [
            { name: 'Model', value: 'Model' },
            { name: 'PersistedModel', value: 'PersistedModel' },
            { name: 'ACL', value: 'ACL' },
            { name: 'AccessToken', value: 'AccessToken' },
            { name: 'Application', value: 'Application' },
            { name: 'Change', value: 'Change' },
            { name: 'Checkpoint', value: 'Checkpoint' },
            { name: 'Email', value: 'Email' },
            { name: 'KeyValueModel', value: 'KeyValueModel' },
            { name: 'Role', value: 'Role' },
            { name: 'RoleMapping', value: 'RoleMapping' },
            { name: 'Scope', value: 'Scope' },
            { name: 'User', value: 'User' },
            { name: '(custom)', value: 'custom' }
        ];
    }
    ExtLB.checkAvailable = function (dir) {
        return !shelljs.test('-e', path.join(dir, 'package.json'));
    };
    ExtLB.prototype.createApp = function (options) {
        console.log('Creating Application', options);
        if (ExtLB.checkAvailable(process.cwd())) {
            process.stdout.write(chalk_1["default"].keyword('limegreen')('Retrieving application template '));
            simpleGit().clone('https://gitlab.com/ExtLB/app-template.git', '.').then(function () {
                process.stdout.write(chalk_1["default"].keyword('limegreen')(' done!\n'));
                shelljs.rm('-fr', '.git');
                if (!_.includes(options.platforms, 'desktop')) {
                    shelljs.rm('-fr', ['config/client/desktop.json', 'resources/moderndesktop', 'app/desktop']);
                }
                if (!_.includes(options.platforms, 'phone')) {
                    shelljs.rm('-fr', ['config/client/phone.json', 'resources/modernphone', 'app/phone']);
                }
                var modifications = {};
                _.filter(shelljs.ls("" + __dirname + path.sep + "app" + path.sep + "*.ts"), function (file) {
                    return !_.endsWith(file, '.d.ts');
                }).map(function (file) {
                    return file.substr(0, file.length - 3);
                }).forEach(function (changer) {
                    modifications[changer] = require("" + changer)["default"];
                });
                var modifiers = {};
                _.each(modifications, function (files) {
                    _.each(files, function (changes, file) {
                        if (_.isUndefined(modifiers[file])) {
                            modifiers[file] = [];
                        }
                        modifiers[file].push(changes);
                    });
                });
                process.stdout.write(chalk_1["default"].keyword('limegreen')("Modifying " + _.keys(modifiers).length + " files "));
                var files = _.keys(modifiers);
                _.each(files, function (file) {
                    var content = null;
                    if (_.endsWith(file, '.json')) {
                        content = jsonfile.readFileSync(file);
                    }
                    else {
                        content = fs.readFileSync(file, { encoding: 'utf8' });
                    }
                    modifiers[file].forEach(function (modifier) {
                        content = modifier(options, content);
                    });
                    process.stdout.write('.');
                    if (_.isNull(content)) {
                        shelljs.rm(file);
                    }
                    else {
                        if (_.endsWith(file, '.json')) {
                            jsonfile.writeFileSync(file, content, { spaces: 2 });
                        }
                        else {
                            fs.writeFileSync(file, content, { encoding: 'utf8' });
                        }
                    }
                });
                process.stdout.write(chalk_1["default"].keyword('limegreen')(' done!\n'));
                console.log(chalk_1["default"].keyword('limegreen')('Installing/Building Modules (npm install)'));
                shelljs.exec('npm install', { silent: false }, function () {
                    console.log(chalk_1["default"].keyword('limegreen')('Modules Installed!'));
                    if (_.includes(options.features, 'perspective-admin') ||
                        _.includes(options.features, 'perspective-auth') ||
                        _.includes(options.features, 'perspective-error') ||
                        _.includes(options.features, 'perspective-main') ||
                        _.includes(options.features, 'auth')) {
                        var perspectives_1 = _.filter(options.features, function (feature) {
                            return _.startsWith(feature, 'perspective-');
                        }).map(function (feature) {
                            return "@extlb/" + feature;
                        });
                        var installs = {};
                        if (perspectives_1.length !== 0) {
                            installs.perspectives = function (cb) {
                                process.stdout.write(chalk_1["default"].keyword('limegreen')("Installing " + perspectives_1.length + " Perspectives "));
                                shelljs.exec("npm install " + perspectives_1.join(' ') + " -S", { silent: true }, function (err) {
                                    if (err) {
                                        console.error(err);
                                        cb(err);
                                    }
                                    else {
                                        process.stdout.write(chalk_1["default"].keyword('limegreen')(' done!\n'));
                                        cb();
                                    }
                                });
                            };
                        }
                        if (_.includes(options.features, 'auth')) {
                            if (options.authType === 'users') {
                                installs.authentication = function (cb) {
                                    process.stdout.write(chalk_1["default"].keyword('limegreen')('Installing Authentication Module '));
                                    shelljs.exec('npm install @extlb/auth -S', { silent: true }, function (err) {
                                        if (err) {
                                            console.error(err);
                                            cb(err);
                                        }
                                        else {
                                            process.stdout.write(chalk_1["default"].keyword('limegreen')(' done!\n'));
                                            cb();
                                        }
                                    });
                                };
                            }
                            else if (options.authType === 'accounts') {
                                installs.authentication = function (cb) {
                                    process.stdout.write(chalk_1["default"].keyword('limegreen')('Installing Accounts Module '));
                                    shelljs.exec('npm install @extlb/module-admin-accounts -S', { silent: true }, function (err) {
                                        if (err) {
                                            console.error(err);
                                            cb(err);
                                        }
                                        else {
                                            process.stdout.write(chalk_1["default"].keyword('limegreen')(' done!\n'));
                                            cb();
                                        }
                                    });
                                };
                            }
                        }
                        installs.grunt = function (cb) {
                            process.stdout.write(chalk_1["default"].keyword('limegreen')('Building application '));
                            shelljs.exec('grunt "Build Testing Desktop"', { silent: true }, function (err) {
                                if (err) {
                                    console.error(err);
                                    cb(err);
                                }
                                else {
                                    process.stdout.write(chalk_1["default"].keyword('limegreen')(' done!\n'));
                                    cb();
                                }
                            });
                        };
                        async.series(installs, function (err) {
                            if (err) {
                                console.log(err);
                            }
                            else {
                                console.log(chalk_1["default"].keyword('limegreen')('Install Completed!'));
                            }
                        });
                    }
                });
            })["catch"](function (err) {
                console.error(err);
            });
        }
        else {
            console.log(chalk_1["default"].red('package.json already exists!\n'));
        }
    };
    ExtLB.prototype.createModule = function (options) {
        console.log('Creating Module', options);
        if (ExtLB.checkAvailable(process.cwd())) {
            process.stdout.write(chalk_1["default"].keyword('limegreen')('Retrieving module template '));
            simpleGit().clone('https://gitlab.com/ExtLB/module-template.git', '.').then(function () {
                process.stdout.write(chalk_1["default"].keyword('limegreen')(' done!\n'));
                shelljs.rm('-fr', '.git');
                var modifications = {};
                _.filter(shelljs.ls("" + __dirname + path.sep + "module" + path.sep + "*.ts"), function (file) {
                    return !_.endsWith(file, '.d.ts');
                }).map(function (file) {
                    return file.substr(0, file.length - 3);
                }).forEach(function (changer) {
                    modifications[changer] = require("" + changer)["default"];
                });
                var modifiers = {};
                _.each(modifications, function (files) {
                    _.each(files, function (changes, file) {
                        if (_.isUndefined(modifiers[file])) {
                            modifiers[file] = [];
                        }
                        modifiers[file].push(changes);
                    });
                });
                process.stdout.write(chalk_1["default"].keyword('limegreen')("Modifying " + _.keys(modifiers).length + " files "));
                var files = _.keys(modifiers);
                _.each(files, function (file) {
                    var content = null;
                    if (_.endsWith(file, '.json')) {
                        content = jsonfile.readFileSync(file);
                    }
                    else {
                        content = fs.readFileSync(file, { encoding: 'utf8' });
                    }
                    modifiers[file].forEach(function (modifier) {
                        content = modifier(options, content);
                    });
                    process.stdout.write('.');
                    if (_.isNull(content)) {
                        shelljs.rm(file);
                    }
                    else {
                        if (_.endsWith(file, '.json')) {
                            jsonfile.writeFileSync(file, content, { spaces: 2 });
                        }
                        else {
                            fs.writeFileSync(file, content, { encoding: 'utf8' });
                        }
                    }
                });
                process.stdout.write(chalk_1["default"].keyword('limegreen')(' done!\n'));
                process.stdout.write(chalk_1["default"].keyword('limegreen')('Relocating 2 paths '));
                shelljs.mv("app" + path.sep + "desktop" + path.sep + "src" + path.sep + "view" + path.sep + "template", "app" + path.sep + "desktop" + path.sep + "src" + path.sep + "view" + path.sep + options.name);
                process.stdout.write('.');
                shelljs.mv("locales" + path.sep + "en-US" + path.sep + "mod-template.json", "locales" + path.sep + "en-US" + path.sep + "mod-" + options.name + ".json");
                process.stdout.write('.');
                process.stdout.write(chalk_1["default"].keyword('limegreen')(' done!\n'));
                console.log(chalk_1["default"].keyword('limegreen')('Installing/Building Modules (npm install)'));
                shelljs.exec('npm install', { silent: false }, function () {
                    console.log(chalk_1["default"].keyword('limegreen')('Modules Installed!'));
                });
            })["catch"](function (err) {
                console.error(err);
            });
        }
        else {
            console.log(chalk_1["default"].red('package.json already exists!\n'));
        }
    };
    ExtLB.prototype.createPerspective = function (options) {
        console.log('Creating Perspective', options);
        if (ExtLB.checkAvailable(process.cwd())) {
            process.stdout.write(chalk_1["default"].keyword('limegreen')('Retrieving perspective template '));
            simpleGit().clone('https://gitlab.com/ExtLB/perspective-template.git', '.').then(function () {
                process.stdout.write(chalk_1["default"].keyword('limegreen')(' done!\n'));
                shelljs.rm('-fr', '.git');
                var modifications = {};
                _.filter(shelljs.ls("" + __dirname + path.sep + "perspective" + path.sep + "*.ts"), function (file) {
                    return !_.endsWith(file, '.d.ts');
                }).map(function (file) {
                    return file.substr(0, file.length - 3);
                }).forEach(function (changer) {
                    modifications[changer] = require("" + changer)["default"];
                });
                var modifiers = {};
                _.each(modifications, function (files) {
                    _.each(files, function (changes, file) {
                        if (_.isUndefined(modifiers[file])) {
                            modifiers[file] = [];
                        }
                        modifiers[file].push(changes);
                    });
                });
                process.stdout.write(chalk_1["default"].keyword('limegreen')("Modifying " + _.keys(modifiers).length + " files "));
                var files = _.keys(modifiers);
                _.each(files, function (file) {
                    var content = null;
                    if (_.endsWith(file, '.json')) {
                        content = jsonfile.readFileSync(file);
                    }
                    else {
                        content = fs.readFileSync(file, { encoding: 'utf8' });
                    }
                    modifiers[file].forEach(function (modifier) {
                        content = modifier(options, content);
                    });
                    process.stdout.write('.');
                    if (_.isNull(content)) {
                        shelljs.rm(file);
                    }
                    else {
                        if (_.endsWith(file, '.json')) {
                            jsonfile.writeFileSync(file, content, { spaces: 2 });
                        }
                        else {
                            fs.writeFileSync(file, content, { encoding: 'utf8' });
                        }
                    }
                });
                process.stdout.write(chalk_1["default"].keyword('limegreen')(' done!\n'));
                process.stdout.write(chalk_1["default"].keyword('limegreen')('Relocating 7 paths '));
                shelljs.mv("app" + path.sep + "desktop" + path.sep + "src" + path.sep + "model" + path.sep + "perspective" + path.sep + "template", "app" + path.sep + "desktop" + path.sep + "src" + path.sep + "model" + path.sep + "perspective" + path.sep + options.name);
                process.stdout.write('.');
                shelljs.mv("app" + path.sep + "desktop" + path.sep + "src" + path.sep + "view" + path.sep + "perspective" + path.sep + "template", "app" + path.sep + "desktop" + path.sep + "src" + path.sep + "view" + path.sep + "perspective" + path.sep + options.name);
                process.stdout.write('.');
                shelljs.mv("locales" + path.sep + "en-US" + path.sep + "per-template.json", "locales" + path.sep + "en-US" + path.sep + "per-" + options.name + ".json");
                process.stdout.write('.');
                shelljs.mv("server" + path.sep + "models" + path.sep + "template-navigation.js", "server" + path.sep + "models" + path.sep + options.name + "-navigation.js");
                process.stdout.write('.');
                shelljs.mv("server" + path.sep + "models" + path.sep + "template-navigation.json", "server" + path.sep + "models" + path.sep + options.name + "-navigation.json");
                process.stdout.write('.');
                shelljs.mv("server" + path.sep + "models" + path.sep + "template-notification.ts", "server" + path.sep + "models" + path.sep + options.name + "-notification.ts");
                process.stdout.write('.');
                shelljs.mv("server" + path.sep + "models" + path.sep + "template-notification.json", "server" + path.sep + "models" + path.sep + options.name + "-notification.json");
                process.stdout.write(chalk_1["default"].keyword('limegreen')(' done!\n'));
                console.log(chalk_1["default"].keyword('limegreen')('Installing/Building Modules (npm install)'));
                shelljs.exec('npm install', { silent: false }, function () {
                    console.log(chalk_1["default"].keyword('limegreen')('Modules Installed!'));
                });
            })["catch"](function (err) {
                console.error(err);
            });
        }
        else {
            console.log(chalk_1["default"].red('package.json already exists!\n'));
        }
    };
    ExtLB.prototype.createModel = function (options) {
        console.log('Creating Model', options);
        if (!ExtLB.checkAvailable(process.cwd())) {
            var modelDir = "" + process.cwd() + path.sep + options.type + path.sep + "models" + path.sep;
            if (!shelljs.test('-d', modelDir)) {
                shelljs.mkdir('-p', modelDir);
            }
            var modelConfigFile = "" + modelDir + options.snakeName + ".json";
            var modelConfig = jsonfile.readFileSync("" + __dirname + path.sep + "model" + path.sep + "config.json");
            modelConfig.name = options.name;
            modelConfig.plural = options.pluralName;
            modelConfig.base = options.baseClass;
            jsonfile.writeFileSync(modelConfigFile, modelConfig, { spaces: 2 });
            var modelCodeFile = "" + modelDir + options.snakeName + ".ts";
            var modelCode = fs.readFileSync("" + __dirname + path.sep + "model" + path.sep + "code.ejs", { encoding: 'utf8' });
            modelCode = ejs.render(modelCode, { options: options });
            fs.writeFileSync(modelCodeFile, modelCode);
            if (shelljs.test('-e', "" + process.cwd() + path.sep + "config" + path.sep + "server" + path.sep + "model-config.json")) {
                var modelsFile = "" + process.cwd() + path.sep + "config" + path.sep + "server" + path.sep + "model-config.json";
                var modelsContent = jsonfile.readFileSync(modelsFile);
                modelsContent[options.name] = { dataSource: options.dataSource, public: options.public };
                jsonfile.writeFileSync(modelsFile, modelsContent, { spaces: 2 });
            }
            else if (shelljs.test('-e', "" + process.cwd() + path.sep + "server" + path.sep + "model-config.json")) {
                var modelsFile = "" + process.cwd() + path.sep + "server" + path.sep + "model-config.json";
                var models = jsonfile.readFileSync(modelsFile);
                if (!_.isObject(models)) {
                    models.models = {};
                }
                models.models[options.name] = { dataSource: options.dataSource, public: options.public };
                jsonfile.writeFileSync(modelsFile, models, { spaces: 2 });
            }
            else {
                console.log(chalk_1["default"].red('model-config.json missing'));
            }
        }
        else {
            console.log(chalk_1["default"].red('package.json missing!\n'));
        }
    };
    ExtLB.prototype.createView = function (options) {
        console.log('Creating View', options);
    };
    return ExtLB;
}());
exports.ExtLB = ExtLB;
var extLoop = new ExtLB();
exports["default"] = extLoop;
//# sourceMappingURL=index.js.map