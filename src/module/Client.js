"use strict";
exports.__esModule = true;
var ejs = require("ejs");
var path = require("path");
var changes = {};
function ejsProcessor(options, content) {
    content = ejs.render(content, { options: options });
    return content;
}
changes["app" + path.sep + "desktop" + path.sep + "src" + path.sep + "view" + path.sep + "template" + path.sep + "View.js"] = ejsProcessor;
changes["app" + path.sep + "desktop" + path.sep + "src" + path.sep + "view" + path.sep + "template" + path.sep + "View.scss"] = ejsProcessor;
changes["app" + path.sep + "desktop" + path.sep + "src" + path.sep + "view" + path.sep + "template" + path.sep + "ViewController.js"] = ejsProcessor;
changes["app" + path.sep + "desktop" + path.sep + "src" + path.sep + "view" + path.sep + "template" + path.sep + "ViewModel.js"] = ejsProcessor;
changes["app" + path.sep + "desktop" + path.sep + "src" + path.sep + "view" + path.sep + "template" + path.sep + "ViewStore.js"] = ejsProcessor;
exports["default"] = changes;
//# sourceMappingURL=Client.js.map