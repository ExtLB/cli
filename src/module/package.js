"use strict";
exports.__esModule = true;
var _ = require("lodash");
exports["default"] = {
    'package.json': function (options, content) {
        content.name = "extlb-module-" + options.name;
        content.description = _.capitalize(options.name) + " Module for ExtLoopback";
        content.keywords.push(_.capitalize(options.name));
        return content;
    }
};
//# sourceMappingURL=package.js.map