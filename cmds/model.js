"use strict";
exports.__esModule = true;
var __1 = require("..");
var Inquirer = require("inquirer");
var path = require("path");
var _ = require("lodash");
var Joi = require("joi");
var pluralize = require("pluralize");
var shelljs = require("shelljs");
var Model = (function () {
    function Model() {
        this.options = {
            name: null,
            camelName: null,
            snakeName: null,
            capitalName: null,
            dataSource: null,
            baseClass: null,
            public: null,
            pluralName: null,
            type: null
        };
    }
    Model.prototype.getName = function () {
        var me = this;
        var curDir = _.last(process.cwd().split(path.sep));
        return Inquirer.prompt([{
                type: 'input',
                name: 'name',
                message: 'Model Name:',
                "default": me.options.name || curDir.toLowerCase(),
                validate: function (name) {
                    if (_.isNull(Joi.validate(name, Joi.string().required(), { convert: false }).error)) {
                        return true;
                    }
                    else {
                        return 'Model Name is required!';
                    }
                }
            }]).then(function (answers) {
            me.options.name = answers.name;
            me.options.camelName = _.camelCase(answers.name);
            me.options.snakeName = _.snakeCase(answers.name);
            me.options.capitalName = me.options.camelName.charAt(0).toUpperCase() + me.options.camelName.substr(1);
            return me.options.name;
        });
    };
    Model.prototype.getDataSource = function () {
        var me = this;
        return Inquirer.prompt([{
                type: 'list',
                name: 'dataSource',
                message: "Select the data-source to attach " + me.options.name + " to?",
                choices: [
                    {
                        name: 'Persisted DataBase (db)',
                        value: 'db'
                    }, {
                        name: 'Memory DataBase (memoryDb)',
                        value: 'memoryDb'
                    }, {
                        name: 'Other',
                        value: 'other'
                    }
                ],
                "default": 'db'
            }]).then(function (answers) {
            if (answers.dataSource === 'other') {
                return Inquirer.prompt([{
                        type: 'input',
                        name: 'dataSource',
                        message: "Enter data-source to attach " + me.options.name + " to:",
                        validate: function (name) {
                            if (_.isNull(Joi.validate(name, Joi.string().required(), { convert: false }).error)) {
                                return true;
                            }
                            else {
                                return 'Data Source name is required!';
                            }
                        }
                    }]).then(function (answers) {
                    me.options.dataSource = answers.dataSource;
                    return answers.dataSource;
                });
            }
            else {
                me.options.dataSource = answers.dataSource;
                return answers.dataSource;
            }
        });
    };
    Model.prototype.getBaseClass = function () {
        var me = this;
        var choices = __1["default"]["default"].models;
        var localModels = null;
        if (shelljs.test('-e', "" + process.cwd() + path.sep + "config" + path.sep + "server" + path.sep + "model-config.json")) {
            localModels = require("" + process.cwd() + path.sep + "config" + path.sep + "server" + path.sep + "model-config.json");
        }
        else if (shelljs.test('-e', "" + process.cwd() + path.sep + "server" + path.sep + "model-config.json")) {
            localModels = require("" + process.cwd() + path.sep + "server" + path.sep + "model-config.json");
        }
        if (!_.isNull(localModels)) {
            _.each(_.keys(localModels), function (localModel) {
                if (_.findIndex(choices, { name: localModel }) === -1) {
                    choices.push({
                        name: localModel,
                        value: localModel
                    });
                }
            });
        }
        return Inquirer.prompt([{
                type: 'list',
                name: 'baseClass',
                message: 'Select model\'s base class:',
                choices: __1["default"]["default"].models,
                "default": 'PersistedModel'
            }]).then(function (answers) {
            if (answers.baseClass === 'custom') {
                return Inquirer.prompt([{
                        type: 'input',
                        name: 'baseClass',
                        message: 'Select model\'s base class:',
                        validate: function (name) {
                            if (_.isNull(Joi.validate(name, Joi.string().required(), { convert: false }).error)) {
                                return true;
                            }
                            else {
                                return 'Base Class name is required!';
                            }
                        }
                    }]).then(function (answers) {
                    me.options.baseClass = answers.baseClass;
                    return answers.baseClass;
                });
            }
            else {
                me.options.baseClass = answers.baseClass;
                return answers.baseClass;
            }
        });
    };
    Model.prototype.getPublic = function () {
        var me = this;
        return Inquirer.prompt([{
                type: 'confirm',
                name: 'public',
                message: "Expose " + me.options.name + " via the REST API?",
                "default": true
            }]).then(function (answers) {
            me.options.public = answers.public;
            return answers.public;
        });
    };
    Model.prototype.getPlural = function () {
        var me = this;
        return Inquirer.prompt([{
                type: 'input',
                name: 'plural',
                message: 'Custom plural form (used to build REST URL):',
                "default": pluralize.plural(me.options.name),
                validate: function (name) {
                    if (_.isNull(Joi.validate(name, Joi.string().required(), { convert: false }).error)) {
                        return true;
                    }
                    else {
                        return 'Plural form is required!';
                    }
                }
            }]).then(function (answers) {
            me.options.pluralName = answers.plural;
            return answers.plural;
        });
    };
    Model.prototype.getType = function () {
        var me = this;
        return Inquirer.prompt([{
                type: 'list',
                name: 'type',
                message: 'Common model or server only?',
                choices: [
                    { name: 'common', value: 'common' },
                    { name: 'server', value: 'server' },
                ],
                "default": 'common'
            }]).then(function (answers) {
            me.options.type = answers.type;
            return answers.type;
        });
    };
    return Model;
}());
exports.command = 'model';
exports.describe = 'Create a new ExtLoop Model';
exports.builder = {};
exports.handler = function (argv) {
    var app = new Model();
    app.getName().then(function () {
        return app.getDataSource();
    }).then(function () {
        return app.getBaseClass();
    }).then(function () {
        return app.getPublic();
    }).then(function () {
        return app.getPlural();
    }).then(function () {
        return app.getType();
    }).then(function () {
        __1["default"]["default"].createModel(app.options);
    })["catch"](function (err) {
        console.error(err);
    });
};
//# sourceMappingURL=model.js.map