'use strict';
require('babel-register')({
});

module.exports = grunt => {
  require('time-grunt')(grunt);
  grunt.initConfig({
    eslint: {
      all: ['index.ts', 'src/**/*.ts']
    },
    exec: {
      tsc: {
        command: 'tsc -d -p .'
      },
      test: {
        command: 'npm test'
      }
    }
  });
  grunt.loadNpmTasks('grunt-eslint');
  grunt.loadNpmTasks('grunt-exec');
  grunt.registerTask('default', ['exec:tsc', 'eslint:all', 'exec:test']);
};
