"use strict";
exports.__esModule = true;
exports["default"] = {
    'extlb.json': function (options, content) {
        content.name = "extlb-perspective-" + options.name;
        content.client = options.client;
        content.server = options.server;
        content.locale = "per-" + options.name;
        content.perspectives[0] = options.name;
        content.models = {};
        content.models[options.capitalName + "Navigation"] = {
            dataSource: 'db',
            public: true
        };
        content.models[options.capitalName + "Notification"] = {
            dataSource: 'db',
            public: true
        };
        return content;
    }
};
//# sourceMappingURL=extlb.js.map