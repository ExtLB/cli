"use strict";
exports.__esModule = true;
var ejs = require("ejs");
exports["default"] = {
    'Gruntfile.js': function (options, content) {
        content = ejs.render(content, { options: options });
        return content;
    }
};
//# sourceMappingURL=Gruntfile.js.map