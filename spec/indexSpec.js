const ExtLB = require('../src/index');
describe('ExtLB', () => {
  it('has createApp function', () => {
    expect(ExtLB.default.createApp).toEqual(jasmine.any(Function));
  });
  it('has createModule function', () => {
    expect(ExtLB.default.createModule).toEqual(jasmine.any(Function));
  });
  it('has createPerspective function', () => {
    expect(ExtLB.default.createPerspective).toEqual(jasmine.any(Function));
  });
});
