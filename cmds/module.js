"use strict";
exports.__esModule = true;
var ExtLB = require("..");
var Inquirer = require("inquirer");
var path = require("path");
var _ = require("lodash");
var Joi = require("joi");
var Module = (function () {
    function Module() {
        this.options = {
            name: null,
            camelName: null,
            snakeName: null,
            capitalName: null,
            server: null,
            client: null
        };
    }
    Module.prototype.getName = function () {
        var me = this;
        var curDir = _.last(process.cwd().split(path.sep));
        return Inquirer.prompt([{
                type: 'input',
                name: 'name',
                message: 'Module Name',
                "default": me.options.name || curDir.toLowerCase(),
                validate: function (name) {
                    if (_.isNull(Joi.validate(name, Joi.string().lowercase().required(), { convert: false }).error)) {
                        return true;
                    }
                    else {
                        return 'Module Name must be lowercase';
                    }
                }
            }]).then(function (answers) {
            me.options.name = answers.name;
            me.options.camelName = _.camelCase(answers.name);
            me.options.snakeName = _.snakeCase(answers.name);
            me.options.capitalName = me.options.camelName.charAt(0).toUpperCase() + me.options.camelName.substr(1);
        });
    };
    Module.prototype.getServer = function () {
        var me = this;
        return Inquirer.prompt([{
                type: 'confirm',
                name: 'server',
                message: 'Enable server side support?',
                "default": true
            }]).then(function (answers) {
            me.options.server = answers.server;
        });
    };
    Module.prototype.getClient = function () {
        var me = this;
        return Inquirer.prompt([{
                type: 'confirm',
                name: 'client',
                message: 'Enable client side support?',
                "default": true,
                choices: ExtLB["default"]["default"].versions
            }]).then(function (answers) {
            me.options.client = answers.client;
        });
    };
    return Module;
}());
exports.command = 'module';
exports.describe = 'Create a new ExtLoop Module';
exports.builder = {};
exports.handler = function (argv) {
    var app = new Module();
    app.getName().then(function () {
        return app.getServer();
    }).then(function () {
        return app.getClient();
    }).then(function () {
        ExtLB["default"]["default"].createModule(app.options);
    })["catch"](function (err) {
        console.error(err);
    });
};
//# sourceMappingURL=module.js.map