import * as ejs from 'ejs';
import * as path from 'path';

let changes: any = {};
function ejsProcessor(options: any, content: any) {
  content = ejs.render(content, {options: options});
  return content;
}
changes['boot.js'] = ejsProcessor;
changes[`locales${path.sep}en-US${path.sep}mod-template.json`] = (options: any, content: any) => {
  content[options.name.toUpperCase()] = options.capitalName;
  return content;
};
export default changes;
