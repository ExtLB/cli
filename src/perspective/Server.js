"use strict";
exports.__esModule = true;
var ejs = require("ejs");
var path = require("path");
var _ = require("lodash");
var changes = {};
function ejsProcessor(options, content) {
    content = ejs.render(content, { options: options });
    return content;
}
changes['boot.js'] = ejsProcessor;
changes['installer.js'] = ejsProcessor;
changes["server" + path.sep + "models" + path.sep + "template-navigation.js"] = ejsProcessor;
changes["server" + path.sep + "models" + path.sep + "template-navigation.json"] = function (options, content) {
    content.name = _.capitalize(options.name) + "Navigation";
    content.pluralName = _.capitalize(options.name) + "Navigation";
    return content;
};
changes["server" + path.sep + "models" + path.sep + "template-notification.ts"] = ejsProcessor;
changes["server" + path.sep + "models" + path.sep + "template-notification.json"] = function (options, content) {
    content.name = _.capitalize(options.name) + "Notification";
    content.pluralName = _.capitalize(options.name) + "Notifications";
    return content;
};
exports["default"] = changes;
//# sourceMappingURL=Server.js.map