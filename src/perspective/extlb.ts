export default {
  'extlb.json': (options: any, content: any) => {
    content.name = `extlb-perspective-${options.name}`;
    content.client = options.client;
    content.server = options.server;
    content.locale = `per-${options.name}`;
    content.perspectives[0] = options.name;
    content.models = {};
    content.models[`${options.capitalName}Navigation`] = {
      dataSource: 'db',
      public: true
    };
    content.models[`${options.capitalName}Notification`] = {
      dataSource: 'db',
      public: true
    };
    return content;
  }
};
