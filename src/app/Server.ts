import * as path from 'path';
import * as ejs from 'ejs';
import * as _ from 'lodash';

let changes: any = {};

changes[`config${path.sep}server${path.sep}config.json`] = (options: any, content: any) => {
  if (!_.includes(options.features, 'socketio')) {
    delete content.socket;
  }
  if (!_.includes(options.features, 'queues')) {
    delete content.queues;
  }
  if (!_.includes(options.features, 'auth')) {
    content.auth.enabled = false;
  }
  if (!_.includes(options.features, 'passportjs')) {
    content.passport = false;
  }
  return content;
};
changes[`config${path.sep}server${path.sep}model-config.json`] = (options: any, content: any) => {
  if (!_.includes(options.features, 'passportjs')) {
    let sources = content._meta.sources;
    content._meta.sources = [sources[0], sources[1], sources[2], sources[3]];
    delete content.applicationCredential;
    delete content.userCredential;
    delete content.userIdentity;
  }
  if (!_.includes(options.features, 'auth')) {
    delete content.accessToken;
    delete content.application;
    delete content.role;
    delete content.roleMapping;
    delete content.user;
  }
  return content;
};
changes[`config${path.sep}server${path.sep}middleware.json`] = (options: any, content: any) => {
  if (!_.includes(options.features, 'auth')) {
    content.auth = {};
  }
  return content;
};

// Passport JS Models
let passportJSModels: string[] = ['application-credential', 'user-credential', 'user-identity'];
_.each(passportJSModels, (passportJSModel) => {
  changes[`server${path.sep}models${path.sep}${passportJSModel}.json`] = (options: any, content: any) => {
    if (!_.includes(options.features, 'passportjs')) {
      content = null;
    }
    return content;
  };
});

// Core Auth Models
let coreAuthModels: string[] = ['access-token', 'application', 'role', 'role-mapping', 'user'];
_.each(coreAuthModels, (coreAuthModel) => {
  changes[`server${path.sep}models${path.sep}${coreAuthModel}.ts`] = (options: any, content: any) => {
    if (!_.includes(options.features, 'auth')) {
      content = null;
    }
    return content;
  };
  changes[`server${path.sep}models${path.sep}${coreAuthModel}.json`] = (options: any, content: any) => {
    if (!_.includes(options.features, 'auth')) {
      content = null;
    }
    return content;
  };
});

changes[`server${path.sep}middleware${path.sep}auth.js`] = (options: any, content: any) => {
  if (!_.includes(options.features, 'auth')) {
    content = null;
  }
  return content;
};
changes[`server${path.sep}boot${path.sep}queues.ts`] = (options: any, content: any) => {
  if (!_.includes(options.features, 'queues')) {
    content = null;
  }
  return content;
};
changes[`server${path.sep}boot${path.sep}socket.ts`] = (options: any, content: any) => {
  if (options.features.indexOf('socketio') === -1) {
    content = null;
  }
  return content;
};
changes[`server${path.sep}server.js`] = (options: any, content: any) => {
  content = ejs.render(content, {options: options});
  return content;
};
export default changes;
