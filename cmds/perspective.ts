import {Arguments} from 'yargs';
import * as ExtLB from '..'
import * as Inquirer from 'inquirer'
import {Answers} from "inquirer";
import * as path from 'path';
import * as _ from 'lodash';
import * as Joi from 'joi';

class Perspective {
  options: {
    name: string;
    camelName: string;
    snakeName: string;
    capitalName: string;
    server: boolean;
    client: boolean;
  };
  constructor() {
    this.options = {
      name: null,
      camelName: null,
      snakeName: null,
      capitalName: null,
      server: null,
      client: null,
    };
  }
  getName() {
    let me = this;
    let curDir = _.last(process.cwd().split(path.sep));
    return Inquirer.prompt([{
      type: 'input',
      name: 'name',
      message: 'Perspective Name',
      default: me.options.name || curDir.toLowerCase(),
      validate: (name: string) => {
        if (_.isNull(Joi.validate(name, Joi.string().lowercase().required(), {convert: false}).error)) {
          return true;
        } else {
          return 'Perspective Name must be lowercase';
        }
      }
    }]).then((answers: Answers) => {
      me.options.name = answers.name;
      me.options.camelName = _.camelCase(answers.name);
      me.options.snakeName = _.snakeCase(answers.name);
      me.options.capitalName = me.options.camelName.charAt(0).toUpperCase() + me.options.camelName.substr(1);
    });
  }
  getServer() {
    let me = this;
    return Inquirer.prompt([{
      type: 'confirm',
      name: 'server',
      message: 'Enable server side support?',
      default: true
    }]).then((answers: Answers) => {
      me.options.server = answers.server;
    });
  }
  getClient() {
    let me = this;
    return Inquirer.prompt([{
      type: 'confirm',
      name: 'client',
      message: 'Enable client side support?',
      default: true,
    }]).then((answers: Answers) => {
      me.options.client = answers.client;
    });
  }
}

exports.command = 'perspective';
exports.describe = 'Create a new ExtLoop Perspective';
exports.builder = {};
exports.handler = (argv: Arguments) => {
  const app = new Perspective();
  app.getName().then(() => {
    return app.getServer();
  }).then(() => {
    return app.getClient();
  }).then(() => {
    ExtLB.default.default.createPerspective(app.options);
  }).catch((err) => {
    console.error(err);
  });
};
