"use strict";
exports.__esModule = true;
var ExtLB = require("..");
var Inquirer = require("inquirer");
var path = require("path");
var _ = require("lodash");
var Joi = require("joi");
var Perspective = (function () {
    function Perspective() {
        this.options = {
            name: null,
            camelName: null,
            snakeName: null,
            capitalName: null,
            server: null,
            client: null
        };
    }
    Perspective.prototype.getName = function () {
        var me = this;
        var curDir = _.last(process.cwd().split(path.sep));
        return Inquirer.prompt([{
                type: 'input',
                name: 'name',
                message: 'Perspective Name',
                "default": me.options.name || curDir.toLowerCase(),
                validate: function (name) {
                    if (_.isNull(Joi.validate(name, Joi.string().lowercase().required(), { convert: false }).error)) {
                        return true;
                    }
                    else {
                        return 'Perspective Name must be lowercase';
                    }
                }
            }]).then(function (answers) {
            me.options.name = answers.name;
            me.options.camelName = _.camelCase(answers.name);
            me.options.snakeName = _.snakeCase(answers.name);
            me.options.capitalName = me.options.camelName.charAt(0).toUpperCase() + me.options.camelName.substr(1);
        });
    };
    Perspective.prototype.getServer = function () {
        var me = this;
        return Inquirer.prompt([{
                type: 'confirm',
                name: 'server',
                message: 'Enable server side support?',
                "default": true
            }]).then(function (answers) {
            me.options.server = answers.server;
        });
    };
    Perspective.prototype.getClient = function () {
        var me = this;
        return Inquirer.prompt([{
                type: 'confirm',
                name: 'client',
                message: 'Enable client side support?',
                "default": true
            }]).then(function (answers) {
            me.options.client = answers.client;
        });
    };
    return Perspective;
}());
exports.command = 'perspective';
exports.describe = 'Create a new ExtLoop Perspective';
exports.builder = {};
exports.handler = function (argv) {
    var app = new Perspective();
    app.getName().then(function () {
        return app.getServer();
    }).then(function () {
        return app.getClient();
    }).then(function () {
        ExtLB["default"]["default"].createPerspective(app.options);
    })["catch"](function (err) {
        console.error(err);
    });
};
//# sourceMappingURL=perspective.js.map