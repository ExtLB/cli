import * as _ from 'lodash';

export default {
  'package.json': (options: any, content: any) => {
    content.name = `extlb-perspective-${options.name}`;
    content.description = `${_.capitalize(options.name)} Perspective for ExtLoopback`;
    content.keywords[5] = _.capitalize(options.name);
    return content;
  }
};
