"use strict";
exports.__esModule = true;
var ejs = require("ejs");
exports["default"] = {
    'index.html': function (options, content) {
        content = ejs.render(content, { options: options });
        return content;
    }
};
//# sourceMappingURL=index.js.map