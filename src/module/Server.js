"use strict";
exports.__esModule = true;
var ejs = require("ejs");
var path = require("path");
var changes = {};
function ejsProcessor(options, content) {
    content = ejs.render(content, { options: options });
    return content;
}
changes['boot.js'] = ejsProcessor;
changes["locales" + path.sep + "en-US" + path.sep + "mod-template.json"] = function (options, content) {
    content[options.name.toUpperCase()] = options.capitalName;
    return content;
};
exports["default"] = changes;
//# sourceMappingURL=Server.js.map