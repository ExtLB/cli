[![pipeline status](https://gitlab.com/ExtLB/Cli/badges/master/pipeline.svg)](https://gitlab.com/ExtLB/Cli/commits/master)

# ExtLoop Cli (Work In Progress)

CURRENTLY NOT FOR PUBLIC USE!!

ExtLB Cli provides an application to create and manage ExtLB projects.

Based on [Loopback](https://www.npmjs.com/package/loopback) and [Sencha ExtJS](https://www.sencha.com/products/extjs/#overview)

### Prerequisites

Before using ExtLoop Cli you must first authenticate with [Sencha's NPM Registry](https://docs.sencha.com/extjs/6.6.0/guides/getting_started/open_tooling.html)

[Note](https://www.sencha.com/blog/announcing-sencha-ext-js-6-6-with-open-tooling-ga/): npm requires you to replace @ in login with “..”. For example, if your login to support portal is firstname.lastname@sencha.com, your login to npm registry will be firstname.lastname..sencha.com. You can use your existing support portal password.

```
npm login --registry=http://npm.sencha.com --scope=@sencha
```

### Installing

Install Model Decorator Module via npm

```
npm install @extjs/elb -g
```

## Getting Started

### Create new Application

```
mkdir exampleapp
cd exampleapp
elb app
```

### Create new Module

```
mkdir examplemodule
cd examplemodule
elb module
```

## Built With

* [@types/node](https://www.npmjs.com/package/@types/node)

## Versioning

We use [SemVer](http://semver.org/) for versioning.
For the versions available, see the [tags on this repository](https://gitlab.com/ExtLB/Cli/tags). 

## Authors

* **Colton McInroy** - *Initial work* - [GitLab Profile](https://gitlab.com/nextgenagritech)

See also the list of [contributors](https://gitlab.com/ExtLB/Cli/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

