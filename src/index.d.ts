export declare class ExtLB {
    logo: string;
    versions: string[];
    authentication: object[];
    platforms: object[];
    features: object[];
    models: object[];
    constructor();
    static checkAvailable(dir: string): boolean;
    createApp(options: {
        name: string;
        camelName: string;
        snakeName: string;
        capitalName: string;
        description: string;
        version: string;
        authType: string;
        platforms: [string];
        features: [string];
    }): void;
    createModule(options: {
        name: string;
        camelName: string;
        snakeName: string;
        capitalName: string;
        server: boolean;
        client: boolean;
    }): void;
    createPerspective(options: {
        name: string;
        camelName: string;
        snakeName: string;
        capitalName: string;
        server: boolean;
        client: boolean;
    }): void;
    createModel(options: {
        name: string;
        camelName: string;
        snakeName: string;
        capitalName: string;
        pluralName: string;
        dataSource: string;
        baseClass: string;
        public: boolean;
        type: string;
    }): void;
    createView(options: {
        name: string;
        camelName: string;
        snakeName: string;
        capitalName: string;
    }): void;
}
declare let extLoop: ExtLB;
export default extLoop;
