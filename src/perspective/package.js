"use strict";
exports.__esModule = true;
var _ = require("lodash");
exports["default"] = {
    'package.json': function (options, content) {
        content.name = "extlb-perspective-" + options.name;
        content.description = _.capitalize(options.name) + " Perspective for ExtLoopback";
        content.keywords[5] = _.capitalize(options.name);
        return content;
    }
};
//# sourceMappingURL=package.js.map