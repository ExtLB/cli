"use strict";
exports.__esModule = true;
var ejs = require("ejs");
var path = require("path");
var changes = {};
changes["app" + path.sep + "desktop" + path.sep + "src" + path.sep + "controller" + path.sep + "Socket.js"] = function (options, content) {
    if (options.features.indexOf('socketio') === -1) {
        content = null;
    }
    return content;
};
changes['app.js'] = function (options, content) {
    content = ejs.render(content, { options: options });
    return content;
};
exports["default"] = changes;
//# sourceMappingURL=Client.js.map