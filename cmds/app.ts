import {Arguments} from 'yargs';
import * as ExtLB from '..'
import * as Inquirer from 'inquirer'
import {Answers} from "inquirer";
import * as path from 'path';
import * as _ from 'lodash';
import * as Joi from 'joi';

class App {
  options: {
    name: string;
    camelName: string;
    snakeName: string;
    capitalName: string;
    description: string;
    version: string;
    authType: string,
    platforms: [string];
    features: [string];
  };
  constructor() {
    this.options = {
      name: null,
      camelName: null,
      snakeName: null,
      capitalName: null,
      description: null,
      version: null,
      authType: null,
      platforms: null,
      features: null,
    };
  }
  getName() {
    let me = this;
    let curDir = _.last(process.cwd().split(path.sep));
    return Inquirer.prompt([{
      type: 'input',
      name: 'name',
      message: 'Application Name',
      default: me.options.name || curDir.toLowerCase(),
      validate: (name: string) => {
        if (_.isNull(Joi.validate(name, Joi.string().lowercase().required(), {convert: false}).error)) {
          return true;
        } else {
          return 'Application Name must be lowercase';
        }
      }
    }]).then((answers: Answers) => {
      me.options.name = answers.name;
      me.options.camelName = _.camelCase(answers.name);
      me.options.snakeName = _.snakeCase(answers.name);
      me.options.capitalName = me.options.camelName.charAt(0).toUpperCase() + me.options.camelName.substr(1);
    });
  }
  getDescription() {
    let me = this;
    let curDir = _.last(process.cwd().split(path.sep));
    return Inquirer.prompt([{
      type: 'input',
      name: 'description',
      message: 'Application Description',
      default: me.options.name || curDir.toLowerCase()
    }]).then((answers: Answers) => {
      me.options.description = answers.description;
    });
  }
  getVersion() {
    let me = this;
    return Inquirer.prompt([{
      type: 'rawlist',
      name: 'version',
      message: 'ExtLoop Version',
      default: '1',
      choices: ExtLB.default.default.versions,
    }]).then((answers: Answers) => {
      me.options.version = answers.version;
    });
  }
  getPlatforms() {
    let me = this;
    return Inquirer.prompt([{
      type: 'checkbox',
      name: 'platforms',
      message: 'Platform(s)',
      choices: ExtLB.default.default.platforms,
    }]).then((answers: Answers) => {
      me.options.platforms = answers.platforms;
    });
  }
  getFeatures() {
    let me = this;
    return Inquirer.prompt([{
      type: 'checkbox',
      name: 'features',
      message: 'Enable Features',
      pageSize: 20,
      choices: ExtLB.default.default.features,
    }]).then((answers: Answers) => {
      me.options.features = answers.features;
    });
  }
  getAuthentication() {
    let me = this;
    return new Promise((resolve, reject) => {
      if (!_.includes(me.options.features, 'auth')) {
        resolve();
      } else {
        Inquirer.prompt([{
          type: 'rawlist',
          name: 'authType',
          message: 'Authentication Type',
          default: '1',
          choices: ExtLB.default.default.authentication,
        }]).then((answers: Answers) => {
          me.options.authType = answers.authType;
          resolve();
        }).catch((err) => {
          reject(err);
        });
      }
    });
  }
  getPassportJS() {
    let me = this;
    return new Promise((resolve, reject) => {
      if (!_.includes(me.options.features, 'auth')) {
        resolve();
      } else {
        Inquirer.prompt([{
          type: 'confirm',
          name: 'passportjs',
          message: 'Would you like to use passportJS for authentication?',
          default: false
        }]).then((answers: Answers) => {
          if (answers.passportjs === true) {
            me.options.features.push('passportjs');
          }
          resolve();
        }).catch((err) => {
          reject(err);
        });
      }
    });
  }
}

exports.command = 'app';
exports.describe = 'Create a new ExtLoop Application';
exports.builder = {};
exports.handler = (argv: Arguments) => {
  const app = new App();
  app.getName().then(() => {
    return app.getDescription();
  }).then(() => {
    return app.getVersion();
  }).then(() => {
    return app.getPlatforms();
  }).then(() => {
    return app.getFeatures();
  }).then(() => {
    return app.getAuthentication();
  }).then(() => {
    return app.getPassportJS();
  }).then(() => {
    ExtLB.default.default.createApp(app.options);
  }).catch((err) => {
    console.error(err);
  });
};
