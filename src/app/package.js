"use strict";
exports.__esModule = true;
var _ = require("lodash");
exports["default"] = {
    'package.json': function (options, content) {
        content.name = options.name;
        content.description = options.description;
        if (_.includes(options.platforms, 'desktop') && !_.includes(options.platforms, 'phone')) {
            delete content.scripts['phone'];
            delete content.scripts['phone-v'];
            delete content.scripts['build-phone-testing'];
            delete content.scripts['build-phone-production'];
        }
        else if (!_.includes(options.platforms, 'desktop') && _.includes(options.platforms, 'phone')) {
            delete content.scripts['desktop'];
            delete content.scripts['desktop-v'];
            delete content.scripts['build-desktop-testing'];
            delete content.scripts['build-desktop-production'];
            content.extbuild.defaultprofile = 'phone';
        }
        if (!_.includes(options.features, 'socketio')) {
            delete content.dependencies['@types/socket.io'];
            delete content.dependencies['@types/socket.io-redis'];
            delete content.dependencies['socket.io'];
            delete content.dependencies['socket.io-redis'];
        }
        if (!_.includes(options.features, 'browserify')) {
            delete content.devDependencies['grunt-browserify'];
        }
        if (!_.includes(options.features, 'queues')) {
            delete content.dependencies['@types/bull'];
            delete content.dependencies['bull'];
        }
        if (!_.includes(options.features, 'passportjs')) {
            delete content.dependencies['loopback-component-passport'];
            delete content.dependencies['passport-local'];
        }
        return content;
    }
};
//# sourceMappingURL=package.js.map