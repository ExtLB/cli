import * as ejs from 'ejs';
import * as path from 'path';
import * as _ from 'lodash';

let changes: any = {};
function ejsProcessor(options: any, content: any) {
  content = ejs.render(content, {options: options});
  return content;
}
changes['boot.js'] = ejsProcessor;
changes['installer.js'] = ejsProcessor;
changes[`server${path.sep}models${path.sep}template-navigation.js`] = ejsProcessor;
changes[`server${path.sep}models${path.sep}template-navigation.json`] = (options:any, content: any) => {
  content.name = `${_.capitalize(options.name)}Navigation`;
  content.pluralName = `${_.capitalize(options.name)}Navigation`;
  return content;
};
changes[`server${path.sep}models${path.sep}template-notification.ts`] = ejsProcessor;
changes[`server${path.sep}models${path.sep}template-notification.json`] = (options:any, content: any) => {
  content.name = `${_.capitalize(options.name)}Notification`;
  content.pluralName = `${_.capitalize(options.name)}Notifications`;
  return content;
};
export default changes;
